# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config


def server(logger):
    """ Serwer echo zwracajacy otrzymane dane
    logger - mechanizm do logowania wiadomości
    """
    server_address = ('194.29.175.240', 5000)  # TODO: zmienić port!

    # Tworzenie gniazda TCP/IP
    # TODO: wstawić kod tworzacy nowe gniazdo sieciowe
    # TODO: ustawić opcję pozwalajaca na natychmiastowe ponowne użycie gniazda
    #       (zobacz koniec http://docs.python.org/2/library/socket.html)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)




    # Powiazanie gniazda z adresem
    # TODO: powiazać gniazdo z adresem
    s.bind(server_address)

    # Nasłuchiwanie przychodzacych połaczeń
    logger.info(u'tworzę serwer na {0}:{1}'.format(*server_address))
    # TODO: uaktywnić nasłuchiwanie na przychodzace połaćzenia
    s.listen(1)

    try:
        # Nieskończona pętla pozwalajaca obsługiwać dowolna liczbę połaczeń
        # ale tylko jedno na raz
        while True:
            # Czekanie na połaczenie
            logger.info(u'czekam na połaczenie')
            # TODO: stwórz nowe gniazdo dla przychodzacego połaczenia
            #       adres klienta umieść w zmiennej addr
            conn, addr = s.accept()

            logger.info(u'połaczono z {0}:{1}'.format(*addr))

            try:
                # Odebranie danych
                # TODO: odbierz dane od klienta i umieść je w zmiennej data
                data = conn.recv(4096)

                logger.info(u'otrzymano "{0}"'.format(data))

                # Odesłanie odebranych danych spowrotem
                # TODO: odeślij klientowi te same dane, które wcześniej przesłał
                conn.send(data)

                logger.info(u'odesłano wiadomość do klienta')

            finally:
                # Zamknięcie połaczenia
                # TODO: zamknij połaczenie sieciowe
                conn.close()
                logger.info(u'zamknięto połaczenie')

    except KeyboardInterrupt:
        # TODO: użyj wyjatku KeyboardIntterupt jako sygnału do zamknięcia gniazda
        #       i zakończenia działania serwera
        #       zastap słowo pass odpowiednim kodem
        pass


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('echo_server')
    server(logger)
    sys.exit(0)