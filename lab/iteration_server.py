# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config


def server(logger):
    """ Serwer iteracyjny zwracajacy kolejny numer połączenia
    logger - mechanizm do logowania wiadomości
    """
    server_address = ('194.29.175.240', 23456)  # TODO: zmienić port!

    # Ustawienie licznika na zero
    count = 0

    # Tworzenie gniazda TCP/IP
    socket_nasluch = socket.socket()
    socket_nasluch.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Powiązanie gniazda z adresem
    socket_nasluch.bind(server_address)

    # Nasłuchiwanie przychodzących połączeń
    socket_nasluch.listen(1)
    logger.info('utworzę serwer na {0}:{1}'.format(*server_address))

    try:
        while True:
            # Czekanie na połączenie
            logger.info('czekam na połączenie')
            conn, addr = socket_nasluch.accept()

            # Nawiązanie połączenia
            #addr = ('', '')
            logger.info('połączono z {0}:{1}'.format(*addr))

            # Podbicie licznika
            count += 1
            try:
                # Wysłanie wartości licznika do klienta
                conn.sendall('{0}'.format(count))
                logger.info('wysłano {0}'.format(count))

            finally:
                # Zamknięcie połączenia
                conn.close()
                logger.info('zamknięto połączenie')

    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    logging.config.fileConfig('logging.conf')
    logger = logging.getLogger('iteration_server')
    server(logger)
    sys.exit(0)