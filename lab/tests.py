# -*- coding: utf-8 -*-

import socket
import unittest

from echo_client import client


class EchoTestCase(unittest.TestCase):
    """testy dla klienta i serwera echo"""
    sending_msg = u'wysylam "{0}"'
    received_msg = u'odebrano "{0}"'

    def setUp(self):
        """ustawienia dla testow"""
        self.logger = LoggerStub()

    def test_short_message_echo(self):
        """Sprawdza przesylanie krotkich wiadomosci"""
        self.message_echo_test(u'krotka wiadomosc')

    def test_long_message_echo(self):
        """Sprawdza przesylanie dlugich wiadomosci"""
        self.message_echo_test(u'''oto wzglednie dluga wiadomosc
        zaopatrzona w znaki nowych linii

        a nawet puste linie,
        ktore moga siac spustoszenie wsrod blednie
        dzialajacego kodu przetwarzajacego dane z gniazd''')

    def message_echo_test(self, message):
        """Sprawdza przesylanie wiadomosci"""
        self.send_message(message)
        actual_sent, actual_received = self.process_log()
        expected_sent = self.sending_msg.format(message)
        self.assertEqual(expected_sent, actual_sent)

        expected_received = self.received_msg.format(message)
        self.assertEqual(expected_received, actual_received)

    def send_message(self, message):
        """Proba wyslania wiadomosci przy pomocy klienta.
        Raportowanie bledow zwiazanych z gniazdem.
        """
        try:
            client(message, self.logger)
        except socket.error as e:
            if e.errno == 61:
                msg = u'Blad: {0}, czy serwer dziala?'
                self.fail(msg.format(e.strerror))
            else:
                self.fail(u'Nieznany blad: {0}'.format(str(e)))

    def process_log(self):
        """przetwarza dane wylane do logera i zwraca komunikaty zwiazane
        z wyslana i otrzymana wiadomoscia
        """
        if not self.logger.data:
            self.fail(u'Brak danych w loggerze')

        return self.logger.data[1], self.logger.data[2]


class LoggerStub:

    def __init__(self):
        self.data = []

    def info(self, msg):
        self.data.append(msg)


if __name__ == '__main__':
    unittest.main()