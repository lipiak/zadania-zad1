# -*- coding: utf-8 -*-

import socket
import unittest

from sum_client import sum_client


class SuperTesty(unittest.TestCase):

    def setUp(self):
        """ustawienia dla testow"""
        #self.logger = LoggerStub()

    def test_arguments_should_be_numbers(self):
        liczba1 = 'fsdghdfhd'
        liczba2 = 'lkoyiut'

        try:
            sum_client(liczba1, liczba2)

            #jesli nie wyrzucil zadnego wyjatku po drodze to fail
            self.fail('Program powinien sprawdzać poprawność wprowadzanych danych')
        except ValueError:
            pass

        liczba1 = 1.0
        liczba2 = 'lkoyiut'

        try:
            sum_client(liczba1, liczba2)

            #jesli nie wyrzucil zadnego wyjatku po drodze to fail
            self.fail('Program powinien sprawdzać poprawność wprowadzanych danych')
        except ValueError:
            pass

        liczba1 = 'lkjhg'
        liczba2 = 7.0

        try:
            sum_client(liczba1, liczba2)

            #jesli nie wyrzucil zadnego wyjatku po drodze to fail
            self.fail('Program powinien sprawdzać poprawność wprowadzanych danych')
        except ValueError:
            pass

    def test_sum_numbers(self):
        liczba1 = 2.05
        liczba2 = 3.9876587
        oczekiwany_wynik = liczba1 + liczba2
        try:
            wynik = float(sum_client(liczba1, liczba2))
            self.assertEqual(wynik, oczekiwany_wynik)
        except socket.error as e:
            if e.errno == 61:
                msg = u'Blad: {0}, czy serwer dziala?'
                self.fail(msg.format(e.strerror))
            else:
                self.fail(u'Nieznany blad: {0}'.format(str(e)))

    def test_dlugich_liczb(self):
        liczba1 = 2.05758437694376934786873459763987694587945945745
        liczba2 = 3.987658757348968376982707804358706348674397694376734
        oczekiwany_wynik = liczba1 + liczba2
        try:
            wynik = float(sum_client(liczba1, liczba2))
            self.assertAlmostEqual(wynik, oczekiwany_wynik)
        except socket.error as e:
            if e.errno == 61:
                msg = u'Blad: {0}, czy serwer dziala?'
                self.fail(msg.format(e.strerror))
            else:
                self.fail(u'Nieznany blad: {0}'.format(str(e)))


if __name__ == '__main__':
    unittest.main()