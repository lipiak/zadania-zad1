# -*- coding: utf-8 -*-

import socket
import sys
import string

def sum_server():
    """ Serwer iteracyjny zwracajacy kolejny numer połączenia
    logger - mechanizm do logowania wiadomości
    """
    server_address = ('194.29.175.240', 54321)
    #server_address = ('127.0.0.1', 54321)

    # Tworzenie gniazda TCP/IP
    socket_nasluch = socket.socket()
    socket_nasluch.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Powiązanie gniazda z adresem
    socket_nasluch.bind(server_address)

    # Nasłuchiwanie przychodzących połączeń
    socket_nasluch.listen(1)

    try:
        while True:
            # Czekanie na połączenie
            conn, addr = socket_nasluch.accept()

            # Wyzerowanie wyniku dla nowego połączenia
            wynik = 0
            try:
                # Odbieranie pierwszej liczby
                data = conn.recv(2048)

                liczby = string.split(data, ';')

                #zwroc wynik dodawania
                conn.sendall('{0}'.format(float(liczby[0]) + float(liczby[1])))

            finally:
                # Zamknięcie połączenia
                conn.close()

    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    sum_server()
    sys.exit(0)