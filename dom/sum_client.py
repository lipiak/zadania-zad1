# -*- coding: utf-8 -*-

import socket
import sys
import logging
import logging.config
import time

def sum_client(liczba1, liczba2):
    valid = True
    try:
        l1 = float(liczba1)
    except ValueError:
        raise ValueError('Liczba1 not a number')
        valid = False

    try:
        l2 = float(liczba2)
    except ValueError:
        raise ValueError('Liczba2 not a number')
        valid = False

    if valid:
        #server_address = ('127.0.0.1', 54321)
        server_address = ('194.29.175.240', 54321)

        s = socket.socket()

        # Połączenie z gniazdem nasłuchującego serwera

        s.connect(server_address)

        try:

            #wyslij pierwsza liczbe
            s.sendall('{0};{1}'.format(liczba1, liczba2))

            # Odebranie odpowiedzi
            received = s.recv(4096*2*2)

        finally:
            # Zamknięcie połączenia na zakończenie działania
            s.close()

            return received


if __name__ == '__main__':

    valid = False

    while not valid:
        try:
            liczba1 = float(raw_input('Liczba 1: '))
            valid = True
        except ValueError:
            print "Not a number"

    valid = False
    while not valid:
        try:
            liczba2 = float(raw_input('Liczba 2: '))
            valid = True
        except ValueError:
            print "Not a number"

    #message = sys.argv[1]
    print sum_client(liczba1, liczba2)
